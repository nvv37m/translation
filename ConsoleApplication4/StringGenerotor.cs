﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Translation
{
    public class StringGenerator
    {
        private static readonly Lazy<StringGenerator> _lazy = new Lazy<StringGenerator>(
       () => new StringGenerator(), LazyThreadSafetyMode.ExecutionAndPublication);

        public static StringGenerator Instance
        {
            get { return StringGenerator._lazy.Value; }
        }

        private static readonly Random _random = new Random();
        private static readonly Dictionary<int, StringBuilder> _stringBuilders = new Dictionary<int, StringBuilder>();
        private const string Characters = "abcdefghijklmnopqrstuvwxyz";

        private StringGenerator()
        {
        }

        public static string GenerateString(int length)
        {
            StringBuilder result;
            if (!_stringBuilders.TryGetValue(length, out result))
            {
                result = new StringBuilder();
                _stringBuilders[length] = result;
            }

            result.Clear();

            for (int i = 0; i < length; i++)
            {
                result.Append(Characters[_random.Next(Characters.Length)]);
            }

            return result.ToString();
        }
        public static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
    }
}
