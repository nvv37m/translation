﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Translation
{
    public class NewEntry
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public List<IndexEntry> IndexList { get; set; }
    }
    public class IndexEntry
    {
        public int NumberofEntry { get; set; }
    }
}
