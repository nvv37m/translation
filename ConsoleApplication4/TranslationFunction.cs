﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Translation
{
    class TranslationFunction
    {
        
        public List<string> Translator(List<string> originalStrings)
        {
            Dictionary<string, string> generatedDictionary=Dict();
            List<NewEntry> listToSort = GetKeys(generatedDictionary);
            TranslationFunction.SortedList(listToSort);
            List<NewEntry> chosenEntries = FindStrings(originalStrings, listToSort,generatedDictionary);
            List<string> result = TranslatedStrings(originalStrings, chosenEntries);
            return result;
        }
        private static Dictionary<string, string> Dict()
        {
            int i = 0;
            Dictionary<string, string> genDictionary = new Dictionary<string, string>()
            {
                ["a"] = "numb",
                ["bs"] = "more",
                ["cs"] = "norm",
                ["hjt"] ="kl",
                ["nmret"] = "mores"
            };
            //for(i=0; i < 100000; i++)
            //{
            //    int size = StringGenerator.RandomNumber(1, 9);
            //    int size1 = StringGenerator.RandomNumber(1, 20);
            //    string Keystring = StringGenerator.GenerateString(size);
            //    string ValueString = StringGenerator.GenerateString(size1);
            //    Next.Add(Keystring, ValueString);
            //}
          
            return genDictionary;
        }
        private static List<NewEntry> SortedList(List<NewEntry> listOfStrings)
        {
            listOfStrings.Sort(CompareByLength);
            listOfStrings.Reverse();
            return listOfStrings;
        }

        private List<NewEntry> GetKeys(Dictionary<string, string> getKeys)
        {
            List<NewEntry> keys = new List<NewEntry>();
            foreach(KeyValuePair<string,string> entry in getKeys)
            {
                NewEntry transform = new NewEntry();
                transform.Key = entry.Key;
                transform.Value = entry.Value;
                transform.IndexList = new List<IndexEntry>();
                keys.Add(transform);
            }
            return keys;
        }

        private static int CompareByLength(NewEntry x, NewEntry y)
        {
            if (x.Key == null)
            {
                if (y.Key == null)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (y.Key == null)
                {
                    return 1;
                }
                else
                {
                    int retval = x.Key.Length.CompareTo(y.Key.Length);
                    if (retval != 0)
                    {
                        return retval;
                    }
                    
                    else
                    {
                        return x.Key.CompareTo(y.Key);
                    }
                }
            }
        }

        private List<NewEntry> FindStrings(List<string> inStrings, List<NewEntry> keyStrings, Dictionary<string, string> genDictionary)
        {
            int i1;
            for (i1 = 0; i1 <= keyStrings.Count(); i1++)
            {
                int i = 0;
                if (keyStrings.Count != 0)
                {
                    NewEntry keyString = keyStrings[i];
                    foreach (string inString in inStrings)
                    {
                        int end = inString.Length;
                        int start = 0;
                        int count = 0;
                        int at = 0;
                        while ((start <= end) && (at > -1))
                        {
                            count = end - start;
                            at = inString.IndexOf(keyString.Key, start, count);
                            if (at == -1) break;
                            start = at + 1;
                            IndexEntry toChoose = new IndexEntry
                            {
                                NumberofEntry = i
                            };
                            keyString.IndexList.Add(toChoose);
                        }
                        i++;
                    }
                    if (keyString.IndexList.Count() == 0)
                    {
                        keyStrings.Remove(keyString);
                        i1--;
                    }
                    else { }
            };
          }
            return keyStrings;
        }

        private List<string> TranslatedStrings(List<string> oldStrings, List<NewEntry> entries)
        {
            List<string> proceededStrings = new List<string>(oldStrings); 
            foreach (NewEntry entry in entries)
            {
                foreach (IndexEntry indexEntry in entry.IndexList)
                {
                    string translatedString = proceededStrings[indexEntry.NumberofEntry].Replace(entry.Key, entry.Value);
                    proceededStrings.RemoveAt(indexEntry.NumberofEntry);
                    proceededStrings.Insert(indexEntry.NumberofEntry, translatedString);
                }
            }

            return proceededStrings;
        }
    }
}
